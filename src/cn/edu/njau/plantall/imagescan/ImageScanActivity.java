package cn.edu.njau.plantall.imagescan;

import java.util.List;

import cn.edu.njau.plantall.R;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.Toast;
/**
 * 浏览一个文件夹下全部图片的类
 */
public class ImageScanActivity extends Activity implements OnItemClickListener{
	private GridView mGridView;
	private List<Uri> list;
	private ImageAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_image_scan);
		
		mGridView = (GridView) findViewById(R.id.child_grid);
		list = getIntent().getParcelableArrayListExtra(ImageFolderScanActivity.INTENT_EXTRA_FOLDER_ALL_CHILDS_PATH_LIST);
		
		adapter = new ImageAdapter(this, list, mGridView);
		mGridView.setAdapter(adapter);
		mGridView.setOnItemClickListener(this);
	}

	@Override
	public void onBackPressed() {
		Toast.makeText(this, "选中 " + adapter.getSelectItems().size() + " item", Toast.LENGTH_LONG).show();
		super.onBackPressed();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Uri uri = list.get(position);
		Intent intent = new Intent(ImageScanActivity.this, ImageViewActivity.class);
		intent.setData(uri);
		this.startActivity(intent);
	}
}
