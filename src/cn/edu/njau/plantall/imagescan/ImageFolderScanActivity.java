package cn.edu.njau.plantall.imagescan;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import cn.edu.njau.plantall.R;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
/*
 * 展示当前有多少个存放图片的文件夹，以及每个文件夹中有多少图片数目的Activity
 */
public class ImageFolderScanActivity extends Activity {
	// 根据父亲路径的不同,将对应路径下的图片存储起来
	private HashMap<String, List<Uri>> mGruopMap = new HashMap<String, List<Uri>>();
	private List<ImageFolderBean> mFolderList = new ArrayList<ImageFolderBean>();
	private final static int SCAN_OK = 1;
	private ProgressDialog mProgressDialog;
	private ImageFolderAdapter adapter;
	private GridView mGroupGridView;
	
	public final static String INTENT_EXTRA_FOLDER_ALL_CHILDS_PATH_LIST = "intent_extra_folder_all_childs_path_list";
	
	private Handler mHandler = new Handler(){

		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			switch (msg.what) {
			case SCAN_OK:
				//关闭进度条
				mProgressDialog.dismiss();
				
				adapter = new ImageFolderAdapter(ImageFolderScanActivity.this, mFolderList = subGroupOfImage(mGruopMap), mGroupGridView);
				mGroupGridView.setAdapter(adapter);
				break;
			}
		}
		
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_image_folder_scan);
		
		mGroupGridView = (GridView) findViewById(R.id.main_grid);
		
		getImages();
		
		mGroupGridView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				List<Uri> childList = mGruopMap.get(mFolderList.get(position).getFolderName());
				
				Intent intent = new Intent(ImageFolderScanActivity.this, ImageScanActivity.class);
				intent.putParcelableArrayListExtra(INTENT_EXTRA_FOLDER_ALL_CHILDS_PATH_LIST, (ArrayList<Uri>)childList);
				startActivity(intent);
			}
		});
	}


	/**
	 * 利用ContentProvider扫描手机中的图片，此方法在运行在子线程中
	 */
	private void getImages() {
		if(!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
			Toast.makeText(this, "暂无外部存储", Toast.LENGTH_SHORT).show();
			return;
		}
		
		//显示进度条
		mProgressDialog = ProgressDialog.show(this, null, "正在加载...");
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				Uri mImageUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
				ContentResolver mContentResolver = ImageFolderScanActivity.this.getContentResolver();

				//只查询jpeg和png的图片
				Cursor mCursor = mContentResolver.query(mImageUri, null,
						MediaStore.Images.Media.MIME_TYPE + "=? or "
								+ MediaStore.Images.Media.MIME_TYPE + "=?",
						new String[] { "image/jpeg", "image/png" }, MediaStore.Images.Media.DATE_MODIFIED);
				
				while (mCursor.moveToNext()) {
					//获取图片的路径
					String path = mCursor.getString(mCursor
							.getColumnIndex(MediaStore.Images.Media.DATA));
					
					//获取该图片的父路径名
					String parentName = new File(path).getParentFile().getName();

                    int index = mCursor.getColumnIndex(MediaStore.Images.ImageColumns._ID);
                    index = mCursor.getInt(index);
                    Uri uri = Uri.parse("content://media/external/images/media/" + index);
					
					//根据父路径名将图片放入到mGruopMap中
					if (!mGruopMap.containsKey(parentName)) {
						List<Uri> chileList = new ArrayList<Uri>();
						chileList.add(uri);
						mGruopMap.put(parentName, chileList);
					} else {
						mGruopMap.get(parentName).add(uri);
					}
				}
				
				mCursor.close();
				
				//ͨ通知Handler扫描图片完成
				mHandler.sendEmptyMessage(SCAN_OK);
				
			}
		}).start();
		
	}
	
	
	/**
	 * 组装分组界面GridView的数据源，因为我们扫描手机的时候将图片信息放在HashMap中
	 * 所以需要遍历HashMap将数据组装成List
	 * 
	 * @param mGruopMap
	 * @return
	 */
	private List<ImageFolderBean> subGroupOfImage(HashMap<String, List<Uri>> mGruopMap){
		if(mGruopMap.size() == 0){
			return null;
		}
		List<ImageFolderBean> list = new ArrayList<ImageFolderBean>();
		
		Iterator<Map.Entry<String, List<Uri>>> it = mGruopMap.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, List<Uri>> entry = it.next();
			ImageFolderBean imageFolderBean = new ImageFolderBean();
			String key = entry.getKey();
			List<Uri> value = entry.getValue();
			
			imageFolderBean.setFolderName(key);
			imageFolderBean.setImageCounts(value.size());
			imageFolderBean.setFirstImagePath(this, value.get(0));//获取该组的第一张图片
			
			list.add(imageFolderBean);
		}
		
		return list;
	}
}
