package cn.edu.njau.plantall.imagescan;

import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

public class ImageUtil {
	final static String TAG = "ImageUtil";

    /**
     * Loads a bitmap that has been downsampled using sampleSize from a given
     * url.
     */
    public static Bitmap loadDownsampledBitmap(Context context, Uri uri, int sampleSize) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inMutable = true;
        options.inSampleSize = sampleSize;
        return loadBitmap(context, uri, options);
    }
    
    /**
     * Returns the bitmap from the given uri loaded using the given options.
     * Returns null on failure.
     */
    public static Bitmap loadBitmap(Context context, Uri uri, BitmapFactory.Options o) {
        if (uri == null || context == null) {
            throw new IllegalArgumentException("bad argument to loadBitmap");
        }
        InputStream is = null;
        try {
            is = context.getContentResolver().openInputStream(uri);
            return BitmapFactory.decodeStream(is, null, o);
        } catch (FileNotFoundException e) {
            Log.d(TAG, "FileNotFoundException for " + uri, e);
        } finally {
            closeSilently(is);
        }
        return null;
    }
    
    private static void closeSilently(Closeable c) {
        if (c == null)
            return;
        try {
            c.close();
        } catch (IOException t) {
            Log.e(TAG, "close fail ", t);
        }
    }
    
    /**
     * 按指定宽度和高度缩放图片,不保证宽高比例
     *
     * @param bitmap
     * @param w
     * @param h
     * @return
     */
    public static Bitmap zoomBitmap(Bitmap bitmap, int w, int h) {
        if (bitmap == null) {
            return null;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Matrix matrix = new Matrix();
        float scaleWidht = ((float) w / width);
        float scaleHeight = ((float) h / height);
        matrix.postScale(scaleWidht, scaleHeight);
        Bitmap newbmp = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        return newbmp;
    }
    
    public static String getLocalPathFromUri(ContentResolver resolver, Uri uri) {
    	Cursor cursor =null;
    	String urlReturn = null;
    	try {
        cursor = resolver.query(uri, new String[] { MediaStore.Images.Media.DATA }, null, null, null);
        if (cursor == null) {
            return null;
        }
        int index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        urlReturn = cursor.getString(index);
    	} catch (Exception e) {
    		e.printStackTrace();
    	} finally {
    		cursor.close();
    	}
		return urlReturn;
    }
}
