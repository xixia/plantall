package cn.edu.njau.plantall.imagescan;

import java.util.List;

import cn.edu.njau.plantall.R;
import cn.edu.njau.plantall.helper.ViewHolder;
import cn.edu.njau.plantall.imagescan.MeasureSizeImageView.OnMeasureListener;
import cn.edu.njau.plantall.imagescan.NativeImageLoader.NativeImageCallBack;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

/*
 * 首页用来展示当前的Image文件夹的GridView的Adpater
 */
public class ImageFolderAdapter extends BaseAdapter{
	private List<ImageFolderBean> mAllFolserList;
	private Point mPoint = new Point(0, 0);//用来封装ImageView的宽和高的对象
	private GridView mGridView;
	protected LayoutInflater mInflater;
	
	@Override
	public int getCount() {
		if(null == mAllFolserList) return 0;
		return mAllFolserList.size();
	}

	@Override
	public Object getItem(int position) {
		return mAllFolserList.get(position);
	}


	@Override
	public long getItemId(int position) {
		return position;
	}
	
	public ImageFolderAdapter(Context context, List<ImageFolderBean> list, GridView mGridView){
		this.mAllFolserList = list;
		this.mGridView = mGridView;
		mInflater = LayoutInflater.from(context);
	}
	

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageFolderBean imageFolderBean = mAllFolserList.get(position);
		String path = imageFolderBean.getFirstImagePath();
		if(convertView == null){
			convertView = mInflater.inflate(R.layout.adapter_image_folder_grid_item, null);
			MeasureSizeImageView iv = ViewHolder.get(convertView, R.id.iv_ifsa_folder_cover);
			//用来监听ImageView的宽和高
			iv.setOnMeasureListener(new OnMeasureListener() {
				@Override
				public void onMeasureSize(int width, int height) {
					// TODO Auto-generated method stub
					mPoint.set(width, height);
				}
			});
		}
		
		MeasureSizeImageView iv = ViewHolder.get(convertView, R.id.iv_ifsa_folder_cover);
		TextView tvTitle = ViewHolder.get(convertView, R.id.tv_ifsa_folder_title);
		TextView tvCount = ViewHolder.get(convertView, R.id.tv_ifsa_folder_child_count);
		iv.setImageResource(R.drawable.empty);
		tvTitle.setText(imageFolderBean.getFolderName());
		tvCount.setText(Integer.toString(imageFolderBean.getImageCounts()));
		//给ImageView设置路径Tag,这是异步加载图片的小技巧
		iv.setTag(path);
		
		//利用NativeImageLoader类加载本地图片
		Bitmap bitmap = NativeImageLoader.getInstance().loadNativeImage(path, mPoint, new NativeImageCallBack() {
			@Override
			public void onImageLoader(Bitmap bitmap, String path) {
				ImageView mImageView = (ImageView) mGridView.findViewWithTag(path);
				if(bitmap != null && mImageView != null){
					mImageView.setImageBitmap(bitmap);
				}
			}
		});
		
		if(bitmap != null){
			iv.setImageBitmap(bitmap);
		}else{
			iv.setImageResource(R.drawable.empty);
		}
		return convertView;
	}
}
