package cn.edu.njau.plantall.imagescan;

import cn.edu.njau.plantall.R;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;

/**
 * 浏览图片的Activity
 * @author zhaoxin5
 *
 */
public class ImageViewActivity extends Activity{
	final String TAG = "ImageViewActivity";

	private final int MSG_SELECT_PICTURE = 9001;
	private ZoomImageView mZoomView;
	
    @Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_imageview);
		mZoomView = (ZoomImageView) this.findViewById(R.id.iv_iva_zoomview);
		if(null != getIntent().getData()) {
			handleData(getIntent().getData());
		}
	}

    private void handleData(Uri uri){
        mHandler.sendMessage(mHandler.obtainMessage(MSG_SELECT_PICTURE, uri));	
    }
    
	Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case MSG_SELECT_PICTURE:
                Log.d(TAG, "handleMessage--MSG_SELECT_PICTURE");
                Uri uri = (Uri) msg.obj;
                Bitmap bitmap = ImageUtil.loadDownsampledBitmap(ImageViewActivity.this, uri, 2);
                mZoomView.setImageBitmap(bitmap);
            default:
                break;
            }
        }
    };
}
