package cn.edu.njau.plantall.imagescan;

import android.content.Context;
import android.net.Uri;

/**
 * 
 * 这是对应于首页显示的图片分类文件夹的一个模型类
 *
 */
public class ImageFolderBean{
	/**
	 * 文件夹的第一张图片路径
	 */
	private String firstImagePath;
	/**
	 * 文件夹名
	 */
	private String folderName; 
	/**
	 * 文件夹中的图片数
	 */
	private int imageCounts;
	
	public String getFirstImagePath() {
		return firstImagePath;
	}
	public void setFirstImagePath(Context ctx, Uri uri) {
		this.firstImagePath = ImageUtil.getLocalPathFromUri(ctx.getContentResolver(), uri);
	}
	public String getFolderName() {
		return folderName;
	}
	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}
	public int getImageCounts() {
		return imageCounts;
	}
	public void setImageCounts(int imageCounts) {
		this.imageCounts = imageCounts;
	}
}
