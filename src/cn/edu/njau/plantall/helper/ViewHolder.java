package cn.edu.njau.plantall.helper;

import android.util.SparseArray;
import android.view.View;

public class ViewHolder {
    // I added a generic return type to reduce the casting noise in client code
    @SuppressWarnings("unchecked")
    public static <T extends View> T get(View view, int id) {
        SparseArray<View> viewHolder = (SparseArray<View>) view.getTag();
        if (viewHolder == null) {
            viewHolder = new SparseArray<View>();
            view.setTag(viewHolder);
        }
        View childView = viewHolder.get(id);
        if (childView == null) {
            childView = view.findViewById(id);
            viewHolder.put(id, childView);
        }
        return (T) childView;
    }
    
    @SuppressWarnings("unchecked")
	public static <T extends View> T findChildView(View view, int id) {
        View childView = view.findViewById(id);
        return (T) childView;
    }
    
    public static void setTag(View v, int key, Object tag){
        v.setTag(key, tag);
    }
    
    public static Object getTag(View v, int key){
        return v.getTag(key);
    }
}