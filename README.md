
##简单Git操作
* `git remote update`  获取远程分支
* `git rebase origin/master` 将本地分支保持和远程分支同步

##提交代码说明
* 格式
    * [`zhaoxin`][`20150127`][
    * `1.Add README.md file`
    * ]
* 代号
    * `zhaoxin` 赵鑫
    * `zhangxl` 张小亮
    * `zhujc` 朱金诚
    * `litl` 李铁岭

##有用的知识
* 1, 2.5G 360云盘`Android demo`
    * [链接点我](http://yunpan.cn/cKHmrXbMbRyQc)  
    * 提取码 0b43
* 2, [Android 开发环境配置教程](http://jingyan.baidu.com/article/948f592448183bd80ff5f9d1.html?qq-pf-to=pcqq.c2c)
* 3, [Windows下安装Git教程](http://jingyan.baidu.com/article/90895e0fb3495f64ed6b0b50.html?qq-pf-to=pcqq.c2c)
